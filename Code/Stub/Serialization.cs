﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using metier;
using System.Runtime.Serialization.Formatters.Binary;
using System.Runtime.Serialization;
using System.IO;

namespace Stub
{
    public class Serialization : IData
    {
        public Flotte Load()
        {
            Flotte flotte = new Flotte();
            Stream stream = File.Open("data.bin",FileMode.Open);
            BinaryFormatter binary = new BinaryFormatter();
            flotte = (Flotte)binary.Deserialize(stream);
            stream.Close();
            return flotte;
        }
        public void Save(Flotte flotte)
        {
            Stream stream = File.Open("data.bin", FileMode.Create);
            BinaryFormatter binary = new BinaryFormatter();
            binary.Serialize(stream,flotte);
            stream.Close();
        }
    }
}
