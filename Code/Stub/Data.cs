﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.ObjectModel;
using metier;

namespace Stub
{
    public class Data : IData
    {
        /// <summary>
        /// Permet le chargement de données en dur pour différents test d'affichages
        /// </summary>
        /// <returns>une flotte : observableCollection de bateau rempli</returns>
        public Flotte Load()
        {
            //Création des différentes ObservableCollection de Skins pour chaque bateau : 
            SkinBateauListe javelin = new SkinBateauListe();
            javelin.ListSkin.Add(new SkinBateau("Default", "/img;Component/res/Javelin/Javelin.png"));
            javelin.ListSkin.Add(new SkinBateau("Retrofit", "/img;Component/res/Javelin/JavelinRetrofit.png"));
            javelin.ListSkin.Add(new SkinBateau("Blissful", "/img;Component/res/Javelin/JavelinWedding.png"));

            SkinBateauListe hood = new SkinBateauListe();
            hood.ListSkin.Add( new SkinBateau("Default", "/img;Component/res/Hood/Hood.png"));
            hood.ListSkin.Add(new SkinBateau("RoseLovePoem", "/img;Component/res/Hood/HoodWedding.png"));
            hood.ListSkin.Add(new SkinBateau("Sunlit Lady", "/img;Component/res/Hood/HoodSummer.png"));

            SkinBateauListe washington = new SkinBateauListe();
            washington.ListSkin.Add(new SkinBateau("Default", "/img;Component/res/Washington/Washington.png"));

            SkinBateauListe cleveland = new SkinBateauListe();
            cleveland.ListSkin.Add(new SkinBateau("Default", "/img;Component/res/Cleveland/Cleveland.png"));
            cleveland.ListSkin.Add(new SkinBateau("Heart-Throbbing Moment", "/img;Component/res/Cleveland/ClevelandWedding.png"));
            cleveland.ListSkin.Add(new SkinBateau("Devil's Descent", "/img;Component/res/Cleveland/ClevelandHalloween.png"));
            cleveland.ListSkin.Add(new SkinBateau("Gentry Knight", "/img;Component/res/Cleveland/ClevelandKnight.png"));

            SkinBateauListe sandy = new SkinBateauListe();
            sandy.ListSkin.Add(new SkinBateau("Default", "/img;Component/res/Sandy/Sandy.png"));
            sandy.ListSkin.Add(new SkinBateau("Retrofit", "/img;Component/res/Sandy/SandyRetrofit.png"));
            sandy.ListSkin.Add(new SkinBateau("Sandy Clau's", "/img;Component/res/Sandy/SandyChristmas.png"));

            SkinBateauListe erebus = new SkinBateauListe();
            erebus.ListSkin.Add(new SkinBateau("Default", "/img;Component/res/Erebus/Erebus.png"));
            erebus.ListSkin.Add(new SkinBateau("Illusory Happiness", "/img;Component/res/Erebus/ErebusHappi.png"));

            SkinBateauListe terror = new SkinBateauListe();
            terror.ListSkin.Add(new SkinBateau("Default", "/img;Component/res/Terror/Terror.png"));
            terror.ListSkin.Add(new SkinBateau("Halloween Terror","/img;Component/res/Terror/TerrorHallo.png"));

            SkinBateauListe portland = new SkinBateauListe();
            portland.ListSkin.Add(new SkinBateau("Default","/img;Component/res/Portland/Portland.png"));
            portland.ListSkin.Add(new SkinBateau("Retrofit","/img;Component/res/Portland/PortlandKai.png"));
            portland.ListSkin.Add(new SkinBateau("School life","/img;Component/res/Portland/PortlandSchool.png"));

            SkinBateauListe shouhou = new SkinBateauListe();
            shouhou.ListSkin.Add(new SkinBateau("Default", "/img;Component/res/Shouhou/Shouhou.png"));
            shouhou.ListSkin.Add(new SkinBateau("Retrofit", "/img;Component/res/Shouhou/ShouhouKai.png"));
            shouhou.ListSkin.Add(new SkinBateau("Witch", "/img;Component/res/Shouhou/ShouhouWitch.png"));

            SkinBateauListe unicorn = new SkinBateauListe();
            unicorn.ListSkin.Add(new SkinBateau("Default", "/img;Component/res/Unicorn/Unicorn.png"));
            unicorn.ListSkin.Add(new SkinBateau("Amusement Park", "/img;Component/res/Unicorn/UnicornCasual.png"));

            SkinBateauListe enterprise = new SkinBateauListe();
            enterprise.ListSkin.Add(new SkinBateau("Default", "/img;Component/res/Enterprise/Enterprise.png"));
            enterprise.ListSkin.Add(new SkinBateau("Starlight Oath", "/img;Component/res/Enterprise/EnterpriseWedding.png"));
            enterprise.ListSkin.Add(new SkinBateau("Blooming Poeny", "/img;Component/res/Enterprise/EnterpriseBloom.png"));
            //Pour des raisons de lisibilité du code, création des Skills à part :
            string skillJav = "Javelin Raid : \n When firing main gun, 5 % chance to increase own Evasion by 30 % (60 %) for 8 seconds.\n\n Attack Mode EX : \n Every 20 seconds, 40 % (70 %) chance to increase own Torpedo stat by 30 % for 5 seconds and release a powerful barrage, while launching a tight fan - spread of fast torpedoes.\n\n Full Barrage: \nEvery 15(10) times the main battery is fired, triggers Full Barrage - Javelin I(II)";
            string skillHood = "Glory of the royal Navy : \n When firing main gun: 40% (70%) chance to launch a frontal barrage (damage based on skill level), and increase Reload by 20% (40%) for all ships in the Main Fleet for 8 seconds.";
            string skillWashy = "Courageous Shelling : \n  	Every 20 seconds: 40% (70%) chance to fire a strong barrage at enemies. \n\n Night of the Iron Bottom Sound : \n When Health of South Dakota falls under 30%: increases own damage dealt by 5% (20%) and absorbs 30% damage taken by South Dakota (damage taken through South Dakota's skill 'The Strongest Shield' will NOT be transferred). South Dakota also avoids all lethal damage for 5 seconds. Can only occur once per battle.";
            string skillCleve = "Raid Order : \n Every 20 seconds: 25% chance to increase damage dealt by all ships in the fleet by 5% (25%) for 8 seconds. Does not stack with the same skill. \n \n Anti-Air Mode : \n 	When firing Anti-Air Guns: 25% chance to increase own Anti-Air by 20% (40%), and decrease own Firepower by 40% (20%) for 3 seconds. \n\n Full Barrage : \n Every 15 (10) times the main gun is fired, triggers Full Barrage - Cleveland Class I (II). ";
            string skillSandy = "I Am No. 1! : \n When firing Anti-Air Guns, 15% chance to increase fleet's Anti-Air by 30% (50%) for 8 seconds. \n\n(Retrofit) Song of the Stars :  \nIncrease own Anti-Air stat by 10% (25%). Every 20 seconds, 50% (100%) chance to release a powerful barrage. \n \n Full Barrage  : \n Every 12 (8) times the main gun is fired, trigger Full Barrage - Atlanta Class I (II). \n(Upon retrofit) Every 16 times the main gun is fired, trigger Full Barrage - San Diego.";
            string skillErebus = "Infinite Darkness : \nEvery 20 seconds: 30% (60%) chance to fire a special barrage at enemies (damage based on skill level).\n";
            string skillTerror = "Infinite Darkness : \nEvery 20 seconds: 30% (60%) chance to fire a special barrage at enemies (damage based on skill level).\n";
            string skillPort = "Defense Order : \nEvery 20 seconds: 25% chance to reduce damage taken by all ships in the fleet by 5% (15%) for 8 seconds. Does not stack with the same skill.\n\n Full Barrage : \nEvery 12 (8) times the main gun is fired, triggers Full Barrage - Portland Class I (II).";
            string skillShouhou = "Support Carrier :\nWhen launching an airstrike: heals the Escort Fleet for 3.5% (8%) of their max HP.\n\n Air Support : \nAfter every airstrike, increase Air Power of other CVs/CVLs by 5% (15%) for 8 seconds.\n";
            string skillUni = "Support Carrier : \n After launching an airstrike, heal the Escort Fleet for 3.5% (8%) of their max HP.\n\n Reload Vanguard :\nIncrease reload of the Escort Fleet by 5% (15%). Does not stack with other command skills that have similar effect.";
            string skillEnty = "Lucky E :\n When launching an airstrike: 40% (70%) chance to deal double damage with that airstrike, and evade all incoming attacks for 8 seconds.";


            //Création des differents stats des personnages :
            Statistiques statsJav = new Statistiques(1482,"Light",180,65,80,389,244,46,139,0,9,185);
            Statistiques statsHood = new Statistiques(7677,"Medium",138,38,312,0,22,31,285,0,14,56);
            Statistiques statsWashy = new Statistiques(6996,"Heavy",138,89,385,0,18,28,368,0,15,57);
            Statistiques statsCleve = new Statistiques(3880, "Light",164,71,144,0,77,32,285,0,10,146);
            Statistiques statsSandy = new Statistiques(3578, "Light", 185,85,164,146,73,32,516,0,11,150);
            Statistiques statsErebus = new Statistiques(3035, "Light",126,91,247,0,28,12,149,0,10,72);
            Statistiques statsTerror = new Statistiques(3035, "Light",126,91,247,0,28,12,149,0,10,72);
            Statistiques statsPort = new Statistiques(4866, "Medium", 148, 78, 208, 0, 45, 26, 228, 0, 10, 107);
            Statistiques statsShouhou = new Statistiques(3932, "Medium", 159, 24, 0, 0, 50, 28, 251, 304, 10, 65);
            Statistiques statsUni = new Statistiques(3932, "Light", 159, 24, 0, 0, 50, 28, 251, 304, 10, 65);
            Statistiques statsEnty = new Statistiques(5385, "Medium", 115, 93, 0, 0, 49, 32, 291, 394, 13, 95);


            //Création des drops maps des personnages :
            DropMapListe dropJavelin = new DropMapListe();
            dropJavelin.Drops.Add(DropMap.Chap1_Map1);
            dropJavelin.Drops.Add(DropMap.Chap2_Map1);
            dropJavelin.Drops.Add(DropMap.Chap2_Map3);
            dropJavelin.Drops.Add(DropMap.Chap3_Map1);
            dropJavelin.Drops.Add(DropMap.Chap3_Map4);
            dropJavelin.Drops.Add(DropMap.Chap4_Map2);
            dropJavelin.Drops.Add(DropMap.Chap4_Map4);
            DropMapListe dropHood = new DropMapListe();
            dropHood.Drops.Add(DropMap.Chap1_Map2);
            dropHood.Drops.Add(DropMap.Chap1_Map3);
            dropHood.Drops.Add(DropMap.Chap1_Map4);
            dropHood.Drops.Add(DropMap.Chap2_Map2);
            dropHood.Drops.Add(DropMap.Chap3_Map1);
            dropHood.Drops.Add(DropMap.Chap3_Map3);
            dropHood.Drops.Add(DropMap.Chap3_Map4);
            dropHood.Drops.Add(DropMap.Chap4_Map1);
            dropHood.Drops.Add(DropMap.Chap4_Map3);
            DropMapListe dropSandy = new DropMapListe();
            dropSandy.Drops.Add(DropMap.Chap1_Map2);
            dropSandy.Drops.Add(DropMap.Chap1_Map3);
            dropSandy.Drops.Add(DropMap.Chap1_Map4);
            dropSandy.Drops.Add(DropMap.Chap2_Map4);
            dropSandy.Drops.Add(DropMap.Chap3_Map3);
            DropMapListe dropWashy = new DropMapListe();
            dropWashy.Drops.Add(DropMap.Chap1_Map2);
            dropWashy.Drops.Add(DropMap.Chap1_Map4);
            dropWashy.Drops.Add(DropMap.Chap2_Map2);
            dropWashy.Drops.Add(DropMap.Chap3_Map1);
            dropWashy.Drops.Add(DropMap.Chap4_Map2);
            DropMapListe dropCleve = new DropMapListe();
            dropCleve.Drops.Add(DropMap.Chap1_Map3);
            dropCleve.Drops.Add(DropMap.Chap3_Map1);
            dropCleve.Drops.Add(DropMap.Chap3_Map4);
            dropCleve.Drops.Add(DropMap.Chap4_Map4);
            DropMapListe dropEre = new DropMapListe();
            dropEre.Drops.Add(DropMap.Chap2_Map4);
            dropEre.Drops.Add(DropMap.Chap3_Map3);
            dropEre.Drops.Add(DropMap.Chap3_Map3);
            dropEre.Drops.Add(DropMap.Chap3_Map4);
            dropEre.Drops.Add(DropMap.Chap4_Map1);
            dropEre.Drops.Add(DropMap.Chap4_Map3);
            DropMapListe dropTerror = new DropMapListe();
            dropTerror.Drops.Add(DropMap.Chap2_Map4);
            dropTerror.Drops.Add(DropMap.Chap3_Map3);
            dropTerror.Drops.Add(DropMap.Chap3_Map3);
            dropTerror.Drops.Add(DropMap.Chap3_Map4);
            dropTerror.Drops.Add(DropMap.Chap4_Map1);
            dropTerror.Drops.Add(DropMap.Chap4_Map3);
            DropMapListe dropPort = new DropMapListe();
            dropPort.Drops.Add(DropMap.Chap3_Map4);
            dropPort.Drops.Add(DropMap.Chap2_Map2);
            dropPort.Drops.Add(DropMap.Chap3_Map1);
            dropPort.Drops.Add(DropMap.Chap4_Map2);
            dropPort.Drops.Add(DropMap.Chap3_Map3);
            dropPort.Drops.Add(DropMap.Chap4_Map1);
            dropPort.Drops.Add(DropMap.Chap4_Map3);
            DropMapListe dropShouhou = new DropMapListe();
            DropMapListe dropUnicorn = new DropMapListe();
            DropMapListe dropEnty = new DropMapListe();



            //Création des Différents bateau pour les tests en dur :
            Flotte test = new Flotte();
            Bateau bato = new Bateau("Javelin", 101, true, TypeBateau.Destroyer, new TempsConstruction(0, 27, 0), Rarity.Elite, Nationality.Royal_Navy, javelin, "/img;Component/res/Javelin/JavelinChibi.png", skillJav, statsJav,dropJavelin);
            Bateau bato2 = new Bateau("Hood", 129, true, TypeBateau.Battleship, new TempsConstruction(5, 50, 0), Rarity.Super_Rare, Nationality.Royal_Navy, hood, "/img;Component/res/Hood/HoodChibi.png",skillHood,statsHood, dropHood);
            Bateau bato3 = new Bateau("Washington", 64, false, TypeBateau.Battlecruiser, new TempsConstruction(4, 50, 0), Rarity.Super_Rare, Nationality.Eagle_Union, washington, "/img;Component/res/Washington/WashingtonChibi.png",skillWashy,statsWashy, dropWashy);
            Bateau bato4 = new Bateau("Clevebro", 37, true, TypeBateau.Light_Cruiser, new TempsConstruction(1, 25, 0), Rarity.Common, Nationality.Ironblood, cleveland, "/img;Component/res/Cleveland/ClevelandChibi.png", skillCleve,statsCleve, dropCleve);
            Bateau bato5 = new Bateau("Sandy", 36, false, TypeBateau.Light_Cruiser, new TempsConstruction(1, 15, 0), Rarity.Rare, Nationality.Sakura_Empire, sandy, "/img;Component/res/Sandy/SandyChibi.png", skillSandy, statsSandy, dropSandy);
            Bateau bato6 = new Bateau("Erebus", 149, false, TypeBateau.Monitor, new TempsConstruction(1, 0, 0), Rarity.Elite, Nationality.Iris_Libre, erebus,"/img;Component/res/Erebus/ErebusChibi.png", skillErebus, statsErebus, dropEre);
            Bateau bato7 = new Bateau("Terror", 150, false, TypeBateau.Monitor, new TempsConstruction(1, 0, 0), Rarity.Elite, Nationality.Vichya_Dominion, terror, "/img;Component/res/Terror/TerrorChibi.png", skillTerror, statsTerror, dropTerror);
            Bateau bato8 = new Bateau("Portland", 044, true, TypeBateau.Heavy_Cruiser, new TempsConstruction(1, 50, 00), Rarity.Rare, Nationality.Eastern_Radiance, portland, "/img;Component/res/Portland/PortlandChibi.png", skillPort, statsPort, dropPort);
            Bateau bato9 = new Bateau("Shouhou", 222, true, TypeBateau.Light_Aircraft_Carrier, new TempsConstruction(0, 0, 0), Rarity.Rare, Nationality.Sakura_Empire, shouhou, "/img;Component/res/Shouhou/ShouhouChibi.png", skillShouhou, statsShouhou, dropShouhou);
            Bateau bato10 = new Bateau("Unicorn", 142, false, TypeBateau.Light_Aircraft_Carrier, new TempsConstruction(2, 30, 0), Rarity.Elite, Nationality.Eastern_Radiance, unicorn, "/img;Component/res/Unicorn/UnicornChibi.png", skillUni, statsUni, dropUnicorn);
            Bateau bato11 = new Bateau("Enterprise", 077, false, TypeBateau.Aircraft_Carrier, new TempsConstruction(0, 0, 0), Rarity.Common, Nationality.Eagle_Union, enterprise, "/img;Component/res/Enterprise/EnterpriseChibi.png", skillEnty, statsEnty, dropEnty);

            test.Fleet.Add(bato);
            test.Fleet.Add(bato2);
            test.Fleet.Add(bato3);
            test.Fleet.Add(bato4);
            test.Fleet.Add(bato5);
            test.Fleet.Add(bato6);
            test.Fleet.Add(bato7);
            test.Fleet.Add(bato8);
            test.Fleet.Add(bato9);
            test.Fleet.Add(bato10);
            test.Fleet.Add(bato11);

            return test;
        }
        /// <summary>
        /// Permet de charger des TypeBateau pour ainsi faciliter le tri dans la page recherche.xaml
        /// </summary>
        /// <returns> une ObservableCollection contenant des types de bateau pour le tri</returns>
        public void Save(Flotte flotte) {}
    }
}