﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using metier;

namespace Stub
{
    public interface IData
    {
        Flotte Load();
        void Save(Flotte flotte);
    }
}
