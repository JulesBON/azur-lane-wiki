﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using metier;

namespace AzurLane
{
    /// <summary>
    /// Logique d'interaction pour Window1.xaml
    /// </summary>
    public partial class DropEx : Window
    {
        private Flotte fleet = new Flotte();
        public DropEx(Flotte flotte)
        {
            InitializeComponent();
            fleet = flotte;
            DataContext = fleet.Fleet;

        }

        private void BoutonRet(object sender, RoutedEventArgs e)
        {
            MainWindow menu = new MainWindow();
            this.Close();
            menu.Show();
        }

        private void SelectionChapitre(object sender, SelectionChangedEventArgs e)
        {
            if (ComboChapitre.SelectedIndex == 0)
            {
                DataContext = fleet.Fleet;
            }
            else
            {
                DataContext = fleet.Fleet.Where(bato => bato.DropMaps.Drops.Contains((DropMap)(sender as ComboBox).SelectedValue));
            }
        }

        private void InfosBateau(object sender, RoutedEventArgs e)
        {
            InfoBateau infoBat = new InfoBateau(ListBatChap.SelectedItem as Bateau,fleet);
            infoBat.Show();
            this.Close();
        }
    }
}
