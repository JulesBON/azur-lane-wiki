﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace AzurLane
{
    /// <summary>
    /// Logique d'interaction pour VerifSuppr.xaml
    /// </summary>
    public partial class VerifSuppr : Window
    {
        private bool verif;
        public VerifSuppr()
        {
            InitializeComponent();
        }
        public bool Verif()
        {
            return verif;
        }

        private void Oui(object sender, RoutedEventArgs e)
        {
            verif = true;
            this.Close();
        }

        private void Non(object sender, RoutedEventArgs e)
        {
            verif = false;
            this.Close();
        }
    }
}
