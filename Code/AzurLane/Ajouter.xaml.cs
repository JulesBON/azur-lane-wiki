﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using metier;

namespace AzurLane
{
    /// <summary>
    /// Logique d'interaction pour Ajouter.xaml
    /// </summary>
    public partial class Ajouter : Window
    {
        private TempsConstruction time = new TempsConstruction(0, 0, 0);
        private Statistiques stats = new Statistiques(0, "none", 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
        private Bateau bato = new Bateau();

        public Ajouter(Bateau vaisseau)
        {
            InitializeComponent();
            bato = vaisseau;

            DataContext = bato;
            if (bato.Stats == null)
            {
                gridStats.DataContext = stats;
            }
            else
            {
                stats = bato.Stats;
                gridStats.DataContext = stats;
            }
            if (bato.Temps == null)
            {
                heures.DataContext = time;
                minut.DataContext = time;
                secondes.DataContext = time;
            }
            else
            {
                time = bato.Temps;
                heures.DataContext = time;
                minut.DataContext = time;
                secondes.DataContext = time;
            }
            SelecNation(bato);
            SelecClasse(bato);
            SelecRarete(bato);
            retro.IsChecked = bato.Hasretro;


        }

        public Bateau RetourBateau(Bateau barque)
        {
            barque.Nation = (Nationality)ComboNation.SelectedValue;
            barque.Classe = (TypeBateau)ComboRole.SelectedValue;
            barque.Rarete = (Rarity)ComboRarete.SelectedValue;
            barque.Stats = stats;
            barque.Hasretro = (bool)retro.IsChecked;
            barque.Temps = time;
            return barque;
        }

        private void AjouterDropMap(object sender, RoutedEventArgs e)
        {
            AjoutDropMap ajoutDrop = new AjoutDropMap(bato);
            ajoutDrop.ShowDialog();
            bato.DropMaps= ajoutDrop.Stats();

        }
        private void SelecNation(Bateau batal)
        {
            if (batal.Nation == Nationality.Eagle_Union)
            {
                ComboNation.SelectedIndex = 0;
            }
            else if (batal.Nation == Nationality.Eastern_Radiance)
            {
                ComboNation.SelectedIndex = 1;
            }
            else if (batal.Nation == Nationality.Iris_Libre)
            {
                ComboNation.SelectedIndex = 2;
            }
            else if (batal.Nation == Nationality.Ironblood)
            {
                ComboNation.SelectedIndex = 3;
            }
            else if (batal.Nation == Nationality.Royal_Navy)
            {
                ComboNation.SelectedIndex = 4;
            }
            else if (batal.Nation == Nationality.Sakura_Empire)
            {
                ComboNation.SelectedIndex = 5;
            }
            else if (batal.Nation == Nationality.Vichya_Dominion)
            {
                ComboNation.SelectedIndex = 6;
            }
            else
            {
                ComboNation.SelectedIndex = 0;
            }

        }
        private void SelecClasse (Bateau navire)
        {
            switch (navire.Classe)
            {
                case TypeBateau.Destroyer:
                    ComboRole.SelectedIndex = 0;
                    break;
                case TypeBateau.Light_Cruiser:
                    ComboRole.SelectedIndex = 1;
                    break;
                case TypeBateau.Heavy_Cruiser:
                    ComboRole.SelectedIndex = 2;
                    break;
                case TypeBateau.Battleship:
                    ComboRole.SelectedIndex = 3;
                    break;
                case TypeBateau.Battlecruiser:
                    ComboRole.SelectedIndex = 4;
                    break;
                case TypeBateau.Aircraft_Carrier:
                    ComboRole.SelectedIndex = 5;
                    break;
                case TypeBateau.Light_Aircraft_Carrier:
                    ComboRole.SelectedIndex = 6;
                    break;
                case TypeBateau.Monitor:
                    ComboRole.SelectedIndex = 7;
                    break;

            }
        }
        private void SelecRarete(Bateau voilier)
        {
            switch (voilier.Rarete)
            {
                case Rarity.Common:
                    ComboRarete.SelectedIndex = 0;
                    break;
                case Rarity.Rare:
                    ComboRarete.SelectedIndex = 1;
                    break;
                case Rarity.Elite:
                    ComboRarete.SelectedIndex = 2;
                    break;
                case Rarity.Super_Rare:
                    ComboRarete.SelectedIndex = 3;
                    break;
            }
        }

        private void AjoutBateau (object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
