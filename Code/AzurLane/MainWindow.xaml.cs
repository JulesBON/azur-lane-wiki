﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using metier;
using Stub;

namespace AzurLane
{
    /// <summary>
    /// Logique d'interaction pour MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private IData donnee = new Serialization();
        private Flotte armada = new Flotte();
        public MainWindow()
        {
            InitializeComponent();
            armada = donnee.Load();
        }
        public MainWindow(Flotte fleet)
        {
            InitializeComponent();
            armada = fleet;
        }
        private void Quitter(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
        private void Rechercher(object sender, RoutedEventArgs e)
        {
            Rechercher rech = new Rechercher(armada);
            rech.Show();
            this.Close();
        }
        private void DropEx(object sender, RoutedEventArgs e)
        {
            DropEx drop = new DropEx(armada);
            drop.Show();
            this.Close();
        }
    }
}
