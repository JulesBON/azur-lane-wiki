﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using metier;

namespace AzurLane
{
    /// <summary>
    /// Logique d'interaction pour InfoBateau.xaml
    /// </summary>
    public partial class InfoBateau : Window
    {
        private Bateau bato = new Bateau();
        private Flotte armada = new Flotte();
        public InfoBateau(Bateau bateauSelec, Flotte escadre)
        {
            InitializeComponent();
            armada = escadre;
            DataContext = bateauSelec;
            ComboBoxSkin.DataContext = bateauSelec.LienImage;
            bato = bateauSelec;
        }

        private void BoutonRet(object sender, RoutedEventArgs e)
        {
            Rechercher rech = new Rechercher(armada);
            this.Close();
            rech.Show();
        }
    }
}
