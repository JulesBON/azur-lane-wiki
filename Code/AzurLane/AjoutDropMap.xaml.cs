﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using metier;

namespace AzurLane
{
    /// <summary>
    /// Logique d'interaction pour AjoutDropMap.xaml
    /// </summary>
    public partial class AjoutDropMap : Window
    {
        private DropMapListe drops = new DropMapListe();
        public AjoutDropMap(Bateau bato)
        {
            InitializeComponent();
            Ischecked(bato);

        }
        public DropMapListe Stats()
        {
            return drops;
        }
        private void AjouterDrops(object sender, RoutedEventArgs e)
        {
            RemplirStats();
            this.Close();
        }
        private void RemplirStats()
        {
            if ((bool)CheckChap1Monde1.IsChecked)
            {
                drops.Drops.Add(DropMap.Chap1_Map1);
            }
            if((bool)CheckChap1Monde2.IsChecked)
            {
                drops.Drops.Add(DropMap.Chap1_Map2);
            }
            if ((bool)CheckChap1Monde3.IsChecked)
            {
                drops.Drops.Add(DropMap.Chap1_Map3);
            }
            if ((bool)CheckChap1Monde4.IsChecked)
            {
                drops.Drops.Add(DropMap.Chap1_Map4);
            }
            if ((bool)CheckChap2Monde1.IsChecked)
            {
                drops.Drops.Add(DropMap.Chap2_Map1);
            }
            if ((bool)CheckChap2Monde2.IsChecked)
            {
                drops.Drops.Add(DropMap.Chap2_Map2);
            }
            if ((bool)CheckChap2Monde3.IsChecked)
            {
                drops.Drops.Add(DropMap.Chap2_Map3);
            }
            if ((bool)CheckChap2Monde4.IsChecked)
            {
                drops.Drops.Add(DropMap.Chap2_Map4);
            }
            if ((bool)CheckChap3Monde1.IsChecked)
            {
                drops.Drops.Add(DropMap.Chap3_Map1);
            }
            if ((bool)CheckChap3Monde2.IsChecked)
            {
                drops.Drops.Add(DropMap.Chap3_Map2);
            }
            if ((bool)CheckChap3Monde3.IsChecked)
            {
                drops.Drops.Add(DropMap.Chap3_Map3);
            }
            if ((bool)CheckChap3Monde4.IsChecked)
            {
                drops.Drops.Add(DropMap.Chap3_Map4);
            }
            if ((bool)CheckChap4Monde1.IsChecked)
            {
                drops.Drops.Add(DropMap.Chap4_Map1);
            }
            if ((bool)CheckChap4Monde2.IsChecked)
            {
                drops.Drops.Add(DropMap.Chap4_Map2);
            }
            if ((bool)CheckChap4Monde3.IsChecked)
            {
                drops.Drops.Add(DropMap.Chap4_Map3);
            }
            if ((bool)CheckChap4Monde4.IsChecked)
            {
                drops.Drops.Add(DropMap.Chap4_Map4);
            }
        }
        private void Ischecked(Bateau barque)
        {
            if (barque.DropMaps.Drops.Contains(DropMap.Chap1_Map1))
            {
                CheckChap1Monde1.IsChecked = true;
            }
            if (barque.DropMaps.Drops.Contains(DropMap.Chap1_Map2))
            {
                CheckChap1Monde2.IsChecked = true;
            }
            if (barque.DropMaps.Drops.Contains(DropMap.Chap1_Map3))
            {
                CheckChap1Monde3.IsChecked = true;
            }
            if (barque.DropMaps.Drops.Contains(DropMap.Chap1_Map4))
            {
                CheckChap1Monde4.IsChecked = true;
            }
            if (barque.DropMaps.Drops.Contains(DropMap.Chap2_Map1))
            {
                CheckChap2Monde1.IsChecked = true;
            }
            if (barque.DropMaps.Drops.Contains(DropMap.Chap2_Map2))
            {
                CheckChap2Monde2.IsChecked = true;
            }
            if (barque.DropMaps.Drops.Contains(DropMap.Chap2_Map3))
            {
                CheckChap2Monde3.IsChecked = true;
            }
            if (barque.DropMaps.Drops.Contains(DropMap.Chap2_Map4))
            {
                CheckChap2Monde4.IsChecked = true;
            }
            if (barque.DropMaps.Drops.Contains(DropMap.Chap3_Map1))
            {
                CheckChap3Monde1.IsChecked = true;
            }
            if (barque.DropMaps.Drops.Contains(DropMap.Chap3_Map2))
            {
                CheckChap3Monde2.IsChecked = true;
            }
            if (barque.DropMaps.Drops.Contains(DropMap.Chap3_Map3))
            {
                CheckChap3Monde3.IsChecked = true;
            }
            if (barque.DropMaps.Drops.Contains(DropMap.Chap3_Map4))
            {
                CheckChap3Monde4.IsChecked = true;
            }
            if (barque.DropMaps.Drops.Contains(DropMap.Chap4_Map1))
            {
                CheckChap4Monde1.IsChecked = true;
            }
            if (barque.DropMaps.Drops.Contains(DropMap.Chap4_Map2))
            {
                CheckChap4Monde2.IsChecked = true;
            }
            if (barque.DropMaps.Drops.Contains(DropMap.Chap4_Map3))
            {
                CheckChap4Monde3.IsChecked = true;
            }
            if (barque.DropMaps.Drops.Contains(DropMap.Chap4_Map4))
            {
                CheckChap4Monde4.IsChecked = true;
            }
        }
    }
}
