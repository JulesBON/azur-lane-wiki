﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using metier;

namespace AzurLane
{
    /// <summary>
    /// Logique d'interaction pour UC_ComboR.xaml
    /// </summary>
    public partial class UC_ComboR : UserControl
    {
        private Flotte armada = new Flotte();
        private Rechercher rech;

        public UC_ComboR(Flotte escadre,Rechercher recherche)
        {
            this.rech = recherche;
            armada = escadre;
            rech.recherche.DataContext = armada.Fleet;
            InitializeComponent();                       
            
        }

        private void SelectionChanged_Nation(object sender, SelectionChangedEventArgs e)
        {
            if ((sender as ComboBox).SelectedIndex == 0)
            {
                rech.recherche.DataContext = armada.Fleet;
            }
            else
            {
                Combo_Type.SelectedIndex = 0;
                Combo_Rare.SelectedIndex = 0;
                Combo_Retro.SelectedIndex = 0;
                rech.recherche.DataContext = armada.Fleet.Where(bato => bato.Nation == (Nationality)(sender as ComboBox).SelectedValue);
            }
        }
        private void SelectionChange_Type(object sender, SelectionChangedEventArgs e)
        {
            if ((sender as ComboBox).SelectedIndex == 0)
            {
                rech.recherche.DataContext = armada.Fleet;
            }
            else
            {
                Combo_Nation.SelectedIndex = 0;
                Combo_Rare.SelectedIndex = 0;
                Combo_Retro.SelectedIndex = 0;
                rech.recherche.DataContext = armada.Fleet.Where(bato => bato.Classe == (TypeBateau)(sender as ComboBox).SelectedValue);
            }
        }


        private void SelectionChanged_Rarete(object sender, SelectionChangedEventArgs e)
        {
            if ((sender as ComboBox).SelectedIndex == 0)
            {
                rech.recherche.DataContext = armada.Fleet;
            }
            else
            {
                Combo_Nation.SelectedIndex = 0;
                Combo_Type.SelectedIndex = 0;
                Combo_Retro.SelectedIndex = 0;
                rech.recherche.DataContext = armada.Fleet.Where(bato => bato.Rarete == (Rarity)(sender as ComboBox).SelectedValue);
            }
        }

        private void SelectionChanged_Retrofit(object sender, SelectionChangedEventArgs e)
        {
            if ((sender as ComboBox).SelectedIndex == 0)
            {
                rech.recherche.DataContext = armada.Fleet;
            }
            else
            {
                Combo_Nation.SelectedIndex = 0;
                Combo_Type.SelectedIndex = 0;
                Combo_Rare.SelectedIndex = 0;
                rech.recherche.DataContext = armada.Fleet.Where(bato => bato.Hasretro == (bool)(sender as ComboBox).SelectedValue);
            }
        }
    }
}
