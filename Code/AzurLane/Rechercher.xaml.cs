﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using metier;
using Stub;

namespace AzurLane
{
    /// <summary>
    /// Logique d'interaction pour Window1.xaml
    /// </summary>
    public partial class Rechercher : Window
    {
        private Flotte armada = new Flotte();
        public Rechercher(Flotte escadre)
        {
            InitializeComponent();
            armada = escadre;
            DataContext = armada.Fleet;
            UC_ComboR uc = new UC_ComboR(armada,this);
            rechercher.Children.Add(uc);

        }
        private void Info (object sender, RoutedEventArgs e)
        {
            InfoBateau bat = new InfoBateau(recherche.SelectedItem as Bateau, armada);           
            bat.Show();
            this.Close();
        }

        private void AjouterBateau(object sender, RoutedEventArgs e)
        {

            Bateau newB = new Bateau();
            Ajouter ajout = new Ajouter(newB);
            ajout.ShowDialog();
            newB = ajout.RetourBateau(newB);
            if (newB.Name == null)
            {
                newB.Name = "Unkown";
            }
            newB.LienChibi = "/img;Component/res/Undefined.jpg";
            newB.LienImage.ListSkin.Add(new SkinBateau("Undefined", "/img;Component/res/Undefined.jpg"));
            newB.Skills = "Aucun";
            armada.Fleet.Add(newB);
        }

        private void BoutonRet(object sender, RoutedEventArgs e)
        {
            MainWindow menu = new MainWindow(armada);
            this.Close();
            menu.Show();
        }


      

        private void Sauver(object sender, RoutedEventArgs e)
        {
            Serialization serialize = new Serialization();
            serialize.Save(armada);
        }

        private void ModifierBateau(object sender, RoutedEventArgs e)
        {
            Bateau bato = new Bateau();
            bato = (Bateau)recherche.SelectedItem;
            Ajouter ajout = new Ajouter(bato);
            ajout.ShowDialog();
            bato = ajout.RetourBateau(bato);
            armada.Fleet.Remove((Bateau)recherche.SelectedItem);
            armada.Fleet.Add(bato);
        }

        private void SupprimerBateau(object sender, RoutedEventArgs e)
        {
            bool verification;
            VerifSuppr verif = new VerifSuppr();
            verif.ShowDialog();
            verification= verif.Verif();
            if (verification)
            {
                armada.Fleet.Remove(recherche.SelectedItem as Bateau);
            }
        }
    }
}