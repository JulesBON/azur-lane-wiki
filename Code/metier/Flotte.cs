﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace metier
{
    /// <summary>
    /// ObservableCollection de bateaux
    /// </summary>
    ///

    [Serializable()]
    public class Flotte
    {
        public ObservableCollection<Bateau> Fleet { get; set; } = new ObservableCollection<Bateau>();


    }
}
