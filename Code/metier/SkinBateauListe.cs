﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace metier
{

    [Serializable()]
    public class SkinBateauListe
    { 
        public ObservableCollection<SkinBateau> ListSkin { get; set; } = new ObservableCollection<SkinBateau>();
    }
}