﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace metier
{

    [Serializable()]
    public class Statistiques
    {
        public int Health { get; set; }
        public string Armor { get; set; }
        public int Reload { get; set; }
        public int Luck { get; set; }
        public int Firepower { get; set; }
        public int Torpedo { get; set; }
        public int Evasion { get; set; }
        public int Speed { get; set; }
        public int AntiAir { get; set; }
        public int Aviation { get; set; }
        public int OilConsumption{ get; set; }
        public int Accuracy { get; set; }
        /// <summary>
        /// Constructeur de la classe Statistiques
        /// </summary>
        /// <param name="health">Statistique de sante</param>
        /// <param name="armor"> Le type D'armure du bateau</param>
        /// <param name="reload"> Le rechargement du bateau</param>
        /// <param name="luck">La chance </param>
        /// <param name="firepower">la puissance de feu du bateau</param>
        /// <param name="torpedo">La puissance de ses torpilles</param>
        /// <param name="evasion">Son evasion</param>
        /// <param name="speed">Sa vitesse</param>
        /// <param name="antiAir">Sa puissance anti aerienne</param>
        /// <param name="aviation">Sa puissance d'aviation</param>
        /// <param name="oilConsumption">Sa consommation en petrole par combat</param>
        /// <param name="accuracy">Sa précision</param>
        public Statistiques(int health, string armor, int reload, int luck, int firepower, int torpedo, int evasion, int speed, int antiAir, int aviation, int oilConsumption, int accuracy)
        {
            Health = health;
            Armor = armor;
            Reload = reload;
            Luck = luck;
            Firepower = firepower;
            Torpedo = torpedo;
            Evasion = evasion;
            Speed = speed;
            AntiAir = antiAir;
            Aviation = aviation;
            OilConsumption = oilConsumption;
            Accuracy = accuracy;
        }
    }
}
