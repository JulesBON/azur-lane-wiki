﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace metier
{

    [Serializable()]
    public class SkinBateau
    {
        public string NomSkin { get; set; }
        public string Lien { get; set; }

        /// <summary>
        /// Constructeur de la classe SkinBateau
        /// </summary>
        /// <param name="nomSkin"> Le nom de la tenue en question (defaut, mariage...)</param>
        /// <param name="lien">Le lien vers l'image pour l'afficher</param>
        public SkinBateau(string nomSkin, string lien)
        {
            NomSkin = nomSkin;
            Lien = lien;
        }
    }
}
