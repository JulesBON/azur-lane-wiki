﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace metier
{
    /// <summary>
    /// Enumeration permettant de determiner la nationalité d'un bateau
    /// </summary>
    /// 

    [Serializable()]
    public enum Nationality
    {
        Iris_Libre,
        Eagle_Union,
        Royal_Navy,
        Ironblood,
        Vichya_Dominion,
        Sakura_Empire,
        Eastern_Radiance,
    }
}
