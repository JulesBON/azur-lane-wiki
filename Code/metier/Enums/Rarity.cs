﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;

namespace metier
{
    /// <summary>
    /// Enumeration permettant de determiner la rarete d'un bateau
    /// </summary>
    /// 

    [Serializable()]
    public enum Rarity
    {
        Common,
        Rare,
        Elite,
        Super_Rare
    }
}
