﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace metier
{
    [Serializable()]
    public enum DropMap
    {
        Chap1_Map1,
        Chap1_Map2,
        Chap1_Map3,
        Chap1_Map4,
        Chap2_Map1,
        Chap2_Map2,
        Chap2_Map3,
        Chap2_Map4,
        Chap3_Map1,
        Chap3_Map2,
        Chap3_Map3,
        Chap3_Map4,
        Chap4_Map1,
        Chap4_Map2,
        Chap4_Map3,
        Chap4_Map4
        }
}
