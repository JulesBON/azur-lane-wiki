﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace metier
{
    /// <summary>
    /// Enumeration permettant de definir le type d'un bateau
    /// </summary>
    /// 

    [Serializable()]
    public enum TypeBateau
        {
        Destroyer,
        Battleship,
        Battlecruiser,
        Aircraft_Carrier,
        Light_Aircraft_Carrier,
        Heavy_Cruiser,
        Light_Cruiser,
        Monitor
    }
}
