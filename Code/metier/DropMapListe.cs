﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.ObjectModel;

namespace metier
{
    [Serializable()]
    public class DropMapListe
    {
        public ObservableCollection<DropMap> Drops { get; set; } = new ObservableCollection<DropMap>();
        public override string ToString()
        {
            string message="";
            foreach (DropMap drop in Drops)
            {
                if (drop == DropMap.Chap1_Map1)
                {
                    message = message + " Chapitre 1 Monde 1\n";
                }
                else if (drop == DropMap.Chap1_Map2)
                {
                    message = message + " Chapitre 1 Monde 2\n";
                }
                else if (drop == DropMap.Chap1_Map3)
                {
                    message = message + " Chapitre 1 Monde 3\n";
                }
                else if (drop == DropMap.Chap1_Map4)
                {
                    message = message + " Chapitre 1 Monde 4\n";
                }
                else if (drop == DropMap.Chap2_Map1)
                {
                    message = message + " Chapitre 2 Monde 1\n";
                }
                else if (drop == DropMap.Chap2_Map2)
                {
                    message = message + " Chapitre 2 Monde 2\n";
                }
                else if (drop == DropMap.Chap2_Map3)
                {
                    message = message + " Chapitre 2 Monde 3\n";
                }
                else if (drop == DropMap.Chap2_Map4)
                {
                    message = message + " Chapitre 2 Monde 4\n";
                }
                else if (drop == DropMap.Chap3_Map1)
                {
                    message = message + " Chapitre 3 Monde 1\n";
                }
                else if (drop == DropMap.Chap3_Map2)
                {
                    message = message + " Chapitre 3 Monde 2\n";
                }
                else if (drop == DropMap.Chap3_Map3)
                {
                    message = message + " Chapitre 3 Monde 3\n";
                }
                else if (drop == DropMap.Chap3_Map4)
                {
                    message = message + " Chapitre 3 Monde 4\n";
                }
                else if (drop == DropMap.Chap4_Map1)
                {
                    message = message + " Chapitre 4 Monde 1\n";
                }
                else if (drop == DropMap.Chap4_Map2)
                {
                    message = message + " Chapitre 4 Monde 2\n";
                }
                else if (drop == DropMap.Chap4_Map3)
                {
                    message = message + " Chapitre 4 Monde 3\n";
                }
                else
                {
                    message = message + " Chapitre 4 Monde 4\n";
                }
            }
            return message;
        }
    }
}
