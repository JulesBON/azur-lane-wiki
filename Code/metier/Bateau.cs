﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.ObjectModel;

namespace metier
{
    [Serializable()]
    public class Bateau
    {
        public string Name{ get; set; }
        public int Id { get; set; }
        public bool Hasretro { get; set; }
        public TypeBateau Classe { get; set; }
        public TempsConstruction Temps { get; set; }
        public Rarity Rarete { get; set; }
        public Nationality Nation { get; set; }
        public SkinBateauListe LienImage { get; set; } = new SkinBateauListe();
        public string LienChibi { get; set; }
        public string Skills { get; set; }
        public Statistiques Stats { get; set; }
        public DropMapListe DropMaps { get; set; } = new DropMapListe();

        /// <summary>
        /// Constructeur de la classe bateau 
        /// </summary>
        /// <param name="name"> Nom du bateau en question</param>
        /// <param name="id"> son id</param>
        /// <param name="hasretro">True si le bateau possede une amélioration false sinon</param>
        /// <param name="classe"> le type du bateau en question</param>
        /// <param name="temps">Le temps de construction du bateau</param>
        /// <param name="rarete">La rarete du bateau</param>
        /// <param name="nation">Sa nationalite</param>
        /// <param name="lienImage"> Contient une Observable collection de SkinBateau</param>
        /// <param name="lienChibi">Lien vers l'image par defaut du bateau</param>
        /// <param name="skills">Les differentes Capacite du bateau</param>
        /// <param name="stats">Les statistiques de combat du bateau</param>

        public Bateau() {}

        public Bateau(string name, int id,bool hasretro, TypeBateau classe,TempsConstruction temps,Rarity rarete, Nationality nation, SkinBateauListe lienImage, string lienChibi, string skills, Statistiques stats, DropMapListe dropMaps)
        {
            Name = name;
            Id = id;
            Hasretro = hasretro;
            Classe = classe;
            Temps = temps;
            Rarete = rarete;
            Nation = nation;
            LienImage = lienImage;
            LienChibi = lienChibi;
            Skills = skills;
            Stats = stats;
            DropMaps = dropMaps;
        }

        public override bool Equals(object obj)
        {
            if(object.ReferenceEquals(obj,null))
            {
                return false;
            }
            if (object.ReferenceEquals(this, obj))
            {
                return true;
            }
            if (this.GetType() != obj.GetType())
            {
                return false;
            }
            return this.Equals(obj as Bateau);
        }

        public bool Equals (Bateau bato)
        {
            return this.Id.Equals(bato.Id);
        }
        public override int GetHashCode()
        {
            return Id.GetHashCode()*265;
        }
    }
}
