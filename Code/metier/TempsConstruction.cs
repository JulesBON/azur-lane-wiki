﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace metier
{
    /// <summary>
    /// Classe definissant le temps de construction d'un bateau
    /// </summary>
    /// 

    [Serializable()]
    public class TempsConstruction
    {
        public int Hours { get; set; }
        public int Minutes { get; set; }
        public int Seconds { get; set; }

        /// <summary>
        /// Constructeur de la classe TempsConstruction
        /// </summary>
        /// <param name="hours">Nombre d'heures de la construction</param>
        /// <param name="minutes">Nombre de Minutes de la construction</param>
        /// <param name="seconds">Nombre de seconds de la construction</param>
        public TempsConstruction(int hours, int minutes,int seconds)
        {
            Hours = hours;
            Minutes = minutes;
            Seconds = seconds;
        }


        /// <summary>
        /// Methode permettant l'affichage du temps de construction
        /// </summary>
        /// <returns>Un attribut string contenant l'affichage voulu</returns>
        public override string ToString()
        {
            string message;

            if (Seconds > 60)
            {
                Minutes = Minutes + Seconds / 60;
                Seconds = Seconds % 60;
            }
            if (Minutes > 60)
            {
                Hours = Hours + Minutes / 60;
                Minutes = Minutes % 60;
            }
            if (Hours == 0)
            {
                message = "00";
            }
            else if (Hours < 10)
            {
                message = "0"+Hours.ToString();
            }
            else
                message = "0" + Hours.ToString();
            if (Minutes == 0)
            {
                message = message + ":00";
            }
            else if (Minutes < 10)
            {
                message = message + ":0" + Minutes.ToString();
            }
            else
                message = message + ":" + Minutes.ToString();
            if (Seconds == 0)
            {
                message = message + ":00";
            }
            else
                message = message + ":" + Seconds.ToString();
            return message;
        }
    }
}
